package domain;

import java.util.Stack;
import java.util.Scanner;

public class Main {

	private static Scanner scan = new Scanner(System.in);
	private static Integer esc = 0;
	private static String palavra;

	public static void main(String[] args) {
		System.out.println("VERIFICADOR DE PAL�NDROMOS");
		do {
			System.out.println("(1) Verificar palavra \n(2) Sair");
			esc = scan.nextInt();
			scan.nextLine();
			switch (esc) {
			case 1: {
				verificarPalavra();
				break;
			}
			default:
				System.out.println("At�!");
			}
		} while (esc != 2);

	}

	public static void verificarPalavra() {
		System.out.println("Informe a palavra que deseja verificar: ");
		palavra = scan.nextLine();
		palavra = palavra.replaceAll(palavra, palavra);
		String newPalavra = testaPalavra(palavra);
		if (palavra.equals(newPalavra)) {
			System.out.println("A palavra " + palavra + " � um pal�ndromo!");
		} else {
			System.out.println("A palavra " + palavra + " n�o � um pal�ndromo!");
		}
	}
	
	public static String testaPalavra(String palavra) {
		Stack<Character> letras = new Stack<>();
		for (char letra : palavra.toCharArray()) {
			letras.push(letra);
		}

		StringBuilder sb = new StringBuilder();
		while(letras.size() != 0) {
			sb.append(letras.pop());
		}
		return sb.toString();
	}

}
