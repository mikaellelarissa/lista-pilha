package domain;

import java.util.Scanner;
import java.util.Stack;

public class Main {

	private static Scanner scan = new Scanner(System.in);
	private static Stack<String> pilhaTarefas = new Stack<String>();
	private static Integer esc = 0;
	private static String tarefa;
	
	public static void main(String[] args) {
		System.out.println("TAREFAS");
		do {
			System.out.println("(1) Inserir tarefa \n(2) Pr�xima tarefa \n(3) Sair");
			esc = scan.nextInt();
			scan.nextLine();
			switch (esc) {
			case 1: {
				pilhaTarefas.add(inserirTarefa(tarefa));
				break;
			}
			case 2: {
				proximaTarefa();
				break;
			}
			default:
				System.out.println("At�!");
			}
		} while (esc != 3);

	}

	public static String inserirTarefa(String tarefa) {
		System.out.println("Infome a tarefa que deseja inserir na pilha: ");
		tarefa = scan.nextLine();
		return tarefa;
	}

	public static void proximaTarefa() {
		if (pilhaTarefas.size() > 0) {
			System.out.println(pilhaTarefas.pop());
		} else {
			System.out.println("N�o h� mais tarefas!");
		}
	}
}

