package domain;

import java.util.Stack;
import java.util.Scanner;
import java.util.Iterator;

public class Main {

	private static Scanner scan = new Scanner(System.in);
	private static Stack<Livro> pilhaLivros = new Stack<>();
	private static Integer esc;
	private static String nome;

	public static void main(String[] args) {
		System.out.println("LIVROS");
		do {
			System.out.println("(1) Inserir livro \n(2) Consultar livro \n(3) Remover livro \n(4) Esvaziar pilha \n(5) Sair");
			esc = scan.nextInt();
			scan.nextLine();
			switch (esc) {
			case 1: {
				pilhaLivros.add(construirLivro(nome));
				break;
			}
			case 2: {
				consultarLivro();
				break;
			}
			case 3: {
				removerLivro();
				break;
			}
			case 4: {
				esvaziarPilha();
				break;
			}
			default:
				System.out.println("At�!");
			}
		} while (esc != 5);

	}

	public static Livro construirLivro(String nome) {
		System.out.println("Informe o nome do livro: ");
		nome = scan.nextLine();
		return new Livro(nome);
	}
	
	public static void consultarLivro() {
		System.out.println("(1) Consultar quantidade de livros \n(2) Consultar livro");
		Integer op = scan.nextInt();
		scan.nextLine();
		if(op == 1) {
			System.out.println("A quantidade de livros �: " + pilhaLivros.size());
		} else if(op == 2) {
			System.out.println("Informe o livro que deseja consultar: ");
			nome = scan.nextLine(); 
			for (Livro livro : pilhaLivros) {
				if (livro.getNome().equals(nome)) {
					System.out.println("Livro: " + nome + "\nPosi��o: " + livro);
				} 
			} 
		} else {
			System.out.println("Inv�lido!");
		}
	}
	
	public static void removerLivro() {
		System.out.println("Informe o nome do livro que deseja remover: ");
		nome = scan.nextLine(); 
		Iterator<Livro> it = pilhaLivros.iterator();
		while (it.hasNext()) {
			Livro livro = it.next();
			if (livro.getNome().equals(nome)) {
				pilhaLivros.remove(livro);
			}
		}
	}
	
	public static void esvaziarPilha() {
		pilhaLivros.clear();
		System.out.println("A quantidade de livros na pilha �: " + pilhaLivros.size());
	}

}
