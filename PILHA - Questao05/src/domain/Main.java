package domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

	private static List<Object> pilha;
	private static Scanner scan = new Scanner(System.in);

	public static void main(String[] args) {
		Integer esc;
		do {
			System.out.println("(1)Inicializar pilha \n(2)Adicionar elemento \n(3)Remover \n(4)Sair");
			esc = scan.nextInt();
			scan.nextLine();
			switch (esc) {
			case 1: {
				inicializarPilha();
				break;
			}
			case 2: {
				System.out.println("Informe o que deseja adicionar a pilha: ");
				Object adicionar = scan.nextLine();
				push(pilha, adicionar);
				break;
			}
			case 3: {
				if(pilha.size() > 0) {
					remove(pilha);
				} else {
					System.out.println("Todas as tarefas foram conclu�das!");
				}
				break;
			}
			default:
				System.out.println("At�!");
			}
			if (!pilha.isEmpty()) {
				for (Object elemento : pilha) {
					System.out.println(elemento);
				}
			}
		} while (esc != 4);

	}

	public static void inicializarPilha() {
		System.out.println("Pilha inicializada!");
		pilha = new ArrayList<Object>();
	}

	public static void push(List<Object> list, Object elemento) {
		list.add(elemento);
	}

	public static Object remove(List<Object> list) {
		int posicao= list.size()-1;
		Object aux = list.get(posicao);
		list.remove(posicao);
		return aux;
	}

}
