package domain;

import java.util.Stack;
import java.util.Scanner;

public class Main {

	private static Scanner scan = new Scanner(System.in);
	private static Stack<String> pilha = new Stack<String>();
	private static Integer esc = 0;
	private static String inserir;
	
	public static void main(String[] args) {
		do {
			System.out.println("(1) Inserir \n(2) Pr�ximo \n(3) Sair");
			esc = scan.nextInt();
			scan.nextLine();
			switch (esc) {
			case 1: {
				pilha.push(inicializarPilha(inserir));
				break;
			}
			case 2: {
				proximo();
				break;
			}
			default:
				System.out.println("At�!");
			}
		} while (esc != 3);
		
	}

	public static String inicializarPilha(String inserir) {
		System.out.println("Informe o que deseja guardar: ");
		inserir = scan.nextLine();
		return inserir;
	}
	
	public static void proximo() {
		if(pilha.size() > 0) {
			System.out.println(pilha.pop());
		} else {
			System.out.println("A pilha est� vazia!");
		}
	}
}
